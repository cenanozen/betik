#!/usr/bin/env betik

def makeinc(x)
	return def(y)
		return x+y
	end
end

inc5 = makeinc(5)
inc10 = makeinc(10)

print inc5(5)
print inc10(10)

